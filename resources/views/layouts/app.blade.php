<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">  
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
		<!-- Favicon icon -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon.png') }}"/>	
	
	<!-- Bootstrap -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<!-- Fontawsome -->
    <link href="{{ asset('fonts/css/fontawesome-all.min.css') }}" rel="stylesheet">
    <!-- Animate CSS-->
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <!-- menu CSS-->
    <link href="{{ asset('css/bootstrap-4-navbar.css') }}" rel="stylesheet">
    <!-- menu CSS-->
    <link href="{{ asset('slick/slick.css') }}" rel="stylesheet">
	<!-- Lightbox Gallery -->
    <link href="{{ asset('inc/lightbox/css/jquery.fancybox.css') }}" rel="stylesheet">
    <!-- Preloader CSS-->
    <link href="{{ asset('css/fakeLoader.css') }}" rel="stylesheet">
    <!-- Video popup CSS-->
    <link href="{{ asset('css/magnific-popup.css') }}" rel="stylesheet">
    <!-- Main CSS -->
    <link href="{{ asset('style.css') }}" rel="stylesheet">
    <!-- Color CSS --> 
    <link rel="stylesheet" href="{{ asset('color/color-switcher.css') }}">     
	<!-- Responsive CSS -->
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">  

    <!--begin::Web font -->
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
            google: {"families":["Montserrat:300,400,500,600,700","Tajawal:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <style>
        *{
            font-family: Tajawal ;
        }
        .form-control{
            text-align: right !important;
        }
        .single-course-content{
            text-align: right !important;
        }
        .singlecoursecontent1{
            text-align: left !important;
        }
        .footer-section-1x{
            text-align: right !important;
        }

        .course-single-body{
            text-align: center !important;
        }
    </style>
  </head>
  <body>
  
    <!-- Preloader -->
    <div id="fakeloader"></div>
    
    <div class="alertbox-1x">
		<div class="alert alert-dismissible fade show" role="alert">
			<div class="container">
				<div class="row">				
					<div class="col-md-12">
					  <p>سجل الأن وإحصل على <strong> تخفيض 30 في المئة </strong></p>
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
		  			</div>
				</div>
			</div>
		</div>	
	</div>

    @include('layouts.includes.mainmenu1x')
    
	@yield('content')
	
	@include('layouts.includes.footer')
	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
	<script src="{{ asset('js/popper.min.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<!-- Wow Script -->
	<script src="{{ asset('js/wow.min.js') }}"></script>
	<!-- Counter Script -->
	<script src="{{ asset('js/waypoints.min.js') }}"></script>
	<script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
	<!-- Masonry Portfolio Script -->
    <script src="{{ asset('js/jquery.mixitup.min.js') }}"></script>
	<!-- Lightbox js -->
	<script src="{{ asset('inc/lightbox/js/jquery.fancybox.pack.js') }}"></script>
	<script src="{{ asset('inc/lightbox/js/lightbox.js') }}"></script>
	<!-- loader js-->
    <script src="{{ asset('js/fakeLoader.min.js') }}"></script>
	<!-- Scroll bottom to top -->
	<script src="{{ asset('js/scrolltopcontrol.js') }}"></script>
	<!-- menu -->
	<script src="{{ asset('js/bootstrap-4-navbar.js') }}"></script>
	<!-- youtube popup video -->
	<script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script> 	 
	<!-- Testimonial slider Script -->
	<script src="{{ asset('slick/slick.min.js') }}"></script>
	<!-- Color switcher js -->
	<script src="{{ asset('js/color-switcher.js') }}"></script> 
    <!-- Color-switcher-active -->  
    <script src="{{ asset('js/color-switcher-active.js') }}"></script>
	<!-- Custom script -->
    <script src="{{ asset('js/custom.js') }}"></script>
	
  </body>

</html>