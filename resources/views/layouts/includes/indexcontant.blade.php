<div class="course-1x">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title-middle">
						<h2>أفضل الدروس</h2>
					</div>
				</div>

				<div class="col-md-12">
					<div id="filters" class="course-menu">
			
					  @foreach ($category_all as $category_all_item)
					  	<button class="filter" data-rel="category" data-filter=".category{{ $category_all_item->id }}">{{ $category_all_item->name }}</button>
					  @endforeach
					  <button class="filter active"  data-filter="all">الكل</button>
					</div>
				</div>

				<div class="col-md-12">	
					<div class="all-course">
						<div class="row">
								@foreach ($category_all as $category_all_item)
                    
                        
								@foreach ($category_all_item->tutorial as $category_all_item_tutorial_item)
								<div class="col-md-3 tile category{{ $category_all_item->id }}">
									
									<div class="single-course">
												<div class="hvrbox">
													<img src="{{ $category_all_item_tutorial_item->imageurl }}" alt="{{ $category_all_item_tutorial_item->title }}" class="hvrbox-layer_bottom">
													<div class="hvrbox-layer_top hvrbox-text">
														<div class="hvrbox-text">																	
															<a href="{{ $category_all_item_tutorial_item->videotrialurl }}" class="btn-circle video"><i class="fas fa-play"></i></a><br>
															<a href="/tutorial/{{ $category_all_item_tutorial_item->id }}">معاينة الدورة</a>
														</div>
													</div>
												</div>
												<div class="single-course-content">
													<a href="/tutorial/{{ $category_all_item_tutorial_item->id }}">{{ $category_all_item_tutorial_item->title }}</a>
													<p> {{ $category_all_item_tutorial_item->author->name }} <span><del>{{ $category_all_item_tutorial_item->price }}</del> <b>السعر : {{ $category_all_item_tutorial_item->price }}</b></span></p>
													<h3>
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i> (4)
														<span>المشتركين : {{ count($category_all_item_tutorial_item->purchased) }}</span>
													</h3>
												</div>
									</div>
		
								</div>
								@endforeach
		
							
						@endforeach
						
		
						<div class="col-md-12 text-center">
							<a href="/tutorial" class="btn-small"> عرض المزيد </a>
						</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="achive-goals-1x">
		<div class="container">			
			<div class="col-md-12">
				<div class="title-middle">
					<h2>حقق اهدافك</h2>
					<p>ابحث عن الموضوعات التي تحبها عن طريق تصفح فئات الدورات التدريبية عبر الإنترنت. <br> التعلم عن بعد هو مستقبل التعليم </p>
				</div>
			</div>

			<div class="col-md-12">
				<div class="row">
					<div class="col-md-4">
						<div class="single-goals">
							<img src="{{ asset('images/icon-1.png') }}" alt="slide 1">
							<h4>تطوير المهارات</h4>
							<p>تطوير المهارات تطوير المهارات تطوير المهارات تطوير المهارات </p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="single-goals">
							<img src="{{ asset('images/icon-2.png') }}" alt="slide 1">
							<h4>مجتمع التعلم</h4>
							<p>مجتمع التعلم مجتمع التعلم مجتمع التعلمر مجتمع التعلم</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="single-goals">
							<img src="{{ asset('images/icon-4.png') }}" alt="slide 1">
							<h4>مشروع الحياة الحقيقي</h4>
							<p>مشروع الحياة الحقيقي مشروع الحياة الحقيقي مشروع الحياة الحقيقي</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="learning-path-1x">
		<div class="container">
			<div class="col-md-12">
				<div class="title-middle">
					<h2>المضي قدما في مسارات التعلم</h2>
				</div>
			</div>

			<div class="col-md-12">
				<div class="learning-path">

					<div class="single-learning-path">
						<div class="hvrbox">
							<a href="#"><img src="{{ asset('images/1.jpg') }}" alt="slide 1" class="hvrbox-layer_bottom"></a>
							<div class="hvrbox-layer_top hvrbox-text blue">
								<div class="hvrbox-text">
									<a href="#">كن مصمم</a>							
								</div>
							</div>
						</div>
					</div>
					<div class="single-learning-path">
						<div class="hvrbox">
							<a href="#"><img src="{{ asset('images/4.jpg') }}" alt="slide 1" class="hvrbox-layer_bottom"></a>
							<div class="hvrbox-layer_top hvrbox-text green">
								<div class="hvrbox-text">
									<a href="#">كن مطورًا</a>							
								</div>
							</div>
						</div>
					</div>
					<div class="single-learning-path">
						<div class="hvrbox">
							<a href="#"><img src="{{ asset('images/3.jpg') }}" alt="slide 1" class="hvrbox-layer_bottom"></a>
							<div class="hvrbox-layer_top hvrbox-text red">
								<div class="hvrbox-text">
									<a href="#">كن مدير</a>							
								</div>
							</div>
						</div>
					</div>
					<div class="single-learning-path">
						<div class="hvrbox">
							<a href="#"><img src="{{ asset('images/4.jpg') }}" alt="slide 1" class="hvrbox-layer_bottom"></a>
							<div class="hvrbox-layer_top hvrbox-text yellow">
								<div class="hvrbox-text">
									<a href="#">كن مسوقا</a>							
								</div>
							</div>
						</div>
					</div>
					<div class="single-learning-path">
						<div class="hvrbox">
							<a href="#"><img src="{{ asset('images/5.jpg') }}" alt="slide 1" class="hvrbox-layer_bottom"></a>
							<div class="hvrbox-layer_top hvrbox-text">
								<div class="hvrbox-text">
									<a href="#">مقدمة التصميم</a>							
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
	</div>

	<div class="cta-1x">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="cta-content">
						<h3>هل أنت جاهز للبدء؟</h3>
						<p>ابحث عن الموضوعات التي تحبها عن طريق تصفح فئات الدورات التدريبية عبر الإنترنت <br> ابدأ التعلم مع أفضل الدورات التي تم إنشاؤها باستخدام خبراء </p>
						<a href="#" class="btn-small">ابدأ التدريس</a>
						<a href="#" class="btn-small">ابدا بالتعلم</a>
					</div>
				</div>					
			</div>
		</div>
	</div>