<div class="main-menu-1x">	
    <div class="container">
        <div class="row">
            <div class="col-md-12">		
                <div class="main-menu">		
                    <nav class="navbar navbar-expand-lg navbar-light bg-light btco-hover-menu">
                        <a class="navbar-brand" href="/">
                            <img src="{{ asset('images/logo2.png') }}" class="d-inline-block align-top" alt="">						
                        </a>
                      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                      </button>

                      <div class="collapse navbar-collapse" id="navbarSupportedContent">
                      
                        <ul class="navbar-nav ml-auto main-menu-nav">

                            <li class="nav-item">
                                <a class="nav-link" href="#">المساعدة</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">سياسة الخصوصية</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/tutorial">العروض</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/tutorial">تصفح المواد</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="/">الرئيسية</a>
                            </li>

                            <li class="nav-item sign-in">
                                <a class="nav-link" href="/login">تسجيل الدخول</a>
                            </li>	

                            <li class="nav-item">
                                <a class="btn-small" href="/tutorial">إبدأ الأن</a>
                            </li>	

                        </ul>					
                        
                      </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>