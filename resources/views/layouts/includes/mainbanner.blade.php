<div class="main-banner">           			
    <div class="hvrbox">
        <img src="{{ asset('images/homebanner.jpg') }}" alt="Mountains" class="hvrbox-layer_bottom">
        <div class="hvrbox-layer_top">
            <div class="container">
                <div class="overlay-text text-center">						
                    <h3>المستقبل يبدأ من هنا</h3>
                    <p>العمل بشكل تعاوني ضمان لتحقيق حلم كل شخص<br> اجتماعيا وعاطفيا </p>
                    <div class="col-md-8 offset-md-2">
                        <form action="/tutorial" method="get">
                            <div class="input-group">
                            <input name="textrequest" type="text" class="form-control" aria-label="Text input with dropdown button" placeholder="أدخل كلمة بحثك هنا">
                            <div class="input-group-append styleSelect">
                                <select name="category" id="inputGroupSelect01">
                                        <option selected>جميع الفئات</option>
                                        @foreach ($category_all as $category_all_item)
                                            <option value="{{ $category_all_item->id }}">{{ $category_all_item->name }}</option>
                                        @endforeach
                                </select>
                            </div>
                            <div class="input-group-append">
                                <button class="btn btn-search" type="submit"><img src="{{ asset('images/search.png') }}" alt="Image"></button>
                            </div>
                            </div>
                        </form>
                    </div>

                    <div class="slider-feature">
                        <ul>
                            <li><img src="{{ asset('images/book.png') }}" alt="Mountains"> {{ count($tutorial_all) }} دورة</li>
                            <li><img src="{{ asset('images/cap.png') }}" alt="Mountains"> {{ count($student_all) }} طالب</li>
                            <li><img src="{{ asset('images/man.png') }}" alt="Mountains"> {{ count($author_all) }} مدرس</li>
                        </ul>
                    </div>			
                </div>
            </div>
        </div>
    </div>	                     
</div>