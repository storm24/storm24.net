<div class="course-category-1x">
    <div class="container">
        <div class="row">				
            
            <div style="display:none;">

                <div class="col-md-6">
                    <div class="category-left left-image">
                        <div class="hvrbox">
                            <img src="{{ asset('images/1.jpg') }}" alt="slide 1" class="hvrbox-layer_bottom">
                            <div class="hvrbox-layer_top">
                                <div class="hvrbox-text">
                                    <a href="#">التصميم</a>
                                    <h5>800 درس</h5>								
                                    <a href="#" class="btn-small">عرض التفاصيل</a>
                                </div>
                            </div>
                        </div>				  
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="category-left right-image">
                                <div class="hvrbox">
                                    <img src="{{ asset('images/5.jpg') }}" alt="slide 1" class="hvrbox-layer_bottom">
                                    <div class="hvrbox-layer_top hvrbox-text">
                                        <div class="hvrbox-text">
                                            <a href="#">تطوير تطبيقات الجوال</a>	
                                            <h5>700 درس</h5>							
                                            <a href="#" class="btn-small">عرض التفاصيل</a>
                                        </div>
                                    </div>
                                </div>				  
                            </div>						
                        </div>
                        <div class="col-md-6">
                            <div class="category-left right-image">
                                <div class="hvrbox">
                                    <img src="{{ asset('images/1.jpg') }}" alt="slide 1" class="hvrbox-layer_bottom">
                                    <div class="hvrbox-layer_top hvrbox-text">
                                        <div class="hvrbox-text">
                                            <a href="#">التسويق الرقمي</a>	
                                            <h5>350 درس</h5>							
                                            <a href="#" class="btn-small">عرض التفاصيل</a>
                                        </div>
                                    </div>
                                </div>				  
                            </div>						
                        </div>
                        <div class="col-md-6">
                            <div class="category-left right-image">
                                <div class="hvrbox">
                                    <img src="{{ asset('images/3.jpg') }}" alt="slide 1" class="hvrbox-layer_bottom">
                                    <div class="hvrbox-layer_top hvrbox-text">
                                        <div class="hvrbox-text">
                                            <a href="#">التسويق والمبيعات</a>	
                                            <h5>250 درس</h5>							
                                            <a href="#" class="btn-small">عرض التفاصيل</a>
                                        </div>
                                    </div>
                                </div>				  
                            </div>						
                        </div>
                        <div class="col-md-6">
                            <div class="category-left right-image">
                                <div class="hvrbox">
                                    <img src="{{ asset('images/4.jpg') }}" alt="slide 1" class="hvrbox-layer_bottom">
                                    <div class="hvrbox-layer_top hvrbox-text">
                                        <div class="hvrbox-text">
                                            <a href="#">العمل الحر</a>
                                            <h5>180 درس</h5>							
                                            <a href="#" class="btn-small">عرض التفاصيل</a>
                                        </div>
                                    </div>
                                </div>				  
                            </div>						
                        </div>
                    </div>
                </div>
                
            </div>


            <div class="category-feature">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="single-feature">
                                <img src="{{ asset('images/man-2.png') }}" alt="slide 1">
                                <h4>المدربون الخبراء</h4>
                                <p>نحن نقوم بفحص وتدريب المدربين بدقة للتأكد من كفائتهم  </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="single-feature">
                                <img src="{{ asset('images/clock.png') }}" alt="slide 1">
                                <h4>وصول مدى الحياة</h4>
                                <p>يمكنك الوصول للدروس والمحاضرات في اي وقت</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="single-feature">
                                <img src="{{ asset('images/pc.png') }}" alt="slide 1">
                                <h4>تعلم في أي مكان</h4>
                                <p>التعلم في كل مكان و أي مكان</p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="single-feature">
                                <a href="#" class="btn-small">ابدأ الأن</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>