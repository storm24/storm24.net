<footer class="footer-section-1x">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer-top">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="footer-top-right">														
                               <img src="{{ asset('images/logo.png') }}" alt="slide 1">
                                   <p>من خلال برامج وحزم تدريبية  <br>مصممة خصيصاً لدعم طلاب التعليم <br>العام والتعليم العالي.</p>
                                    <ul>
                                       <li>00966577889922</li>
                                       <li>info@info.com</li>																						
                                     </ul>

                                <div class="footer-social-link">
                                    <ul>
                                        <li><a href="#"> <i class="fab fa-facebook-f"></i> </a></li>	
                                        <li><a href="#"> <i class="fab fa-instagram"></i> </a></li>		
                                        <li><a href="#"> <i class="fab fa-twitter"></i> </a></li>
                                        <li><a href="#"> <i class="fab fa-google-plus-g"></i> </a></li>
                                    </ul>					
                                </div>

                            </div>	
                        </div>

                        <div class="col-md-8">
                            <div class="footer-top-left">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="single-link">
                                            <h3></h3>	
                                            
                                        </div>	
                                    </div>
                                    <div class="col-md-4">
                                        <div class="single-link">
                                            <h3>روابط المساعدة</h3>
                                            <ul>
                                                <li><a href="#">  دعم الطلاب </a></li>
                                                <li><a href="#">  سياسة الدورات </a></li>
                                                <li><a href="#">  الدعم الفني </a></li>
                                                <li><a href="#">  الدفع والشراء </a></li>
                        
                                            </ul>
                                        </div>	
                                    </div>
                                    <div class="col-md-4">
                                        <div class="single-link">
                                            <h3>الدورات</h3>
                                            <ul>
                                                <li><a href="#">  التعليم العام </a></li>
                                                <li><a href="#">  التعليم العالي </a></li>
                                                <li><a href="#">  العمل الحر </a></li>
                                                <li><a href="#">  التصميم </a></li>								
                                            </ul>
                                        </div>	
                                    </div>
                                     

                                </div>
                            </div>	
                        </div>

                    </div>
                </div>	
            </div>

        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <p>© <a href="#"> </a> كل الحقوق محفوظة </p>
                </div>
                <div class="col-md-7">
                    <ul>
                        <li><a href="#">  عنا </a></li>
                        <li><a href="#">  الدعم الفني </a></li>
                        <li><a href="#">  أسئلة شائعة </a></li>
                        <li><a href="#">  تصفح الدورات </a></li>
                        <li><a href="#">  روابط سريعة </a></li>									
                    </ul>
                </div>
            </div>
        </div>						
    </div>	

</footer>