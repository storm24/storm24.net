@extends('layouts.app')

@section('content')

<div class="main-banner">           			
    <div class="hvrbox">
        <img src="{{ asset('images/homebanner.jpg') }}" alt="Mountains" class="hvrbox-layer_bottom">
        <div class="hvrbox-layer_top">
            <div class="container">
                <div class="overlay-text text-center">						
                    <h3> {{ $tutorial->title }} </h3>
                    <p>  </p>

                    <div class="slider-feature">
                        <ul>
                            <li><img src="{{ asset('images/book.png') }}" alt="Mountains"> {{ $tutorial->category->name }} </li>
                            <li><img src="{{ asset('images/cap.png') }}" alt="Mountains"> {{ $tutorial->author->name }} </li>
                        </ul>
                    </div>			
                </div>
            </div>
        </div>
    </div>	                     
</div>

<div class="course-single-body">
	<div class="container">
		<div class="course-info-1x course-info-2x">	
			<div class="row">	
				<div class="col-md-4">
					<div class="course-info-left">
						<div class="media">
						  <img src="{{ asset('images/c6d02f9cec400bee8cff.png') }}" alt="Image">
						  <div class="media-body">
							  <h3>{{ $tutorial->author->name }}</h3>
							<p>{{ $tutorial->author->job }}</p>
						  </div>
						</div>						
					</div>
				</div>	
				<div class="col-md-3">
					<div class="course-info-middle">
						<p>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							&nbsp; 4.9 (66 تقييمات)
						</p>
						<h4>{{ count($tutorial->purchased) }} الطلاب المسجلين</h4>						
					</div>
				</div>
				<div class="col-md-2">
					<div class="course-info-left">
						<div class="media">
						  <div class="media-body">
							  <h4>مدة الدورة</h4>
							<p>{{ $tutorial->total_content_length }} ساعة</p>
						  </div>
						</div>						
					</div>
				</div>
				<div class="col-md-3">
					<div class="course-info-right">
						<h3><del>{{ $tutorial->price }}</del>{{ $tutorial->price }}</h3> ريال
						<a href="#" class="btn-small">إشترك الأن</a>
					</div>
				</div>				

			</div>
		</div>
	</div>

	<div class="container">	
		<div class="course-details-1x">
			<div class="row">
				<div class="col-md-8">
					<div class="course-details-left">

						<div class="course-video" style="background: linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url('{{ $tutorial->imageurl }}');">
							<a href="{{ $tutorial->videotrialurl }}" class="btn-circle video"><i class="fas fa-play"></i></a>
							<h3>معاينة الدورة</h3>
						</div>
					

						<div class="course-menu-nav">
							<ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">

								<li class="nav-item">
											<a class="nav-link" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review" aria-selected="false"><i class="far fa-comment"></i> تقييمات المستخدمين </a>
								</li>
								<li class="nav-item">
										<a class="nav-link" id="instructor-tab" data-toggle="tab" href="#instructor" role="tab" aria-controls="instructor" aria-selected="false"><i class="far fa-user"></i> المدرب</a>
								</li>

							  <li class="nav-item">
									<a class="nav-link" id="curriculum-tab" data-toggle="tab" href="#curriculum" role="tab" aria-controls="curriculum" aria-selected="false"><i class="fas fa-cube"></i> المنهاج </a>
							  </li>
							  <li class="nav-item">
										<a class="nav-link active" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true"> <i class="far fa-file-alt"></i>  نظرة عامة</a>
								</li>
							  
							</ul>
							<div class="tab-content course-menu-tab" id="myTabContent">
							  <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview-tab">
								  <div class="learning-system">
									  <h4>ماذا سوف اتعلم؟</h4>
									  <p> {{ $tutorial->overview_body }} </p>
									</div>
									<div class="price">
											<h5><del>{{ $tutorial->price }}</del><span>{{ $tutorial->price }}</span></h5>							  		
											<a href="#" class="btn-small">إشترك الأن</a>
									</div>
								</div>
								
							  <div class="tab-pane fade" id="curriculum" role="tabpanel" aria-labelledby="curriculum-tab">
										<div class="learning-system">
												<h4>منهاج الدورة</h4>
												<p> {{ $tutorial->curriculum_body }} </p>
											</div>
											<div class="price">
													<h5><del>{{ $tutorial->price }}</del><span>{{ $tutorial->price }}</span></h5>							  		
													<a href="#" class="btn-small">إشترك الأن</a>
											</div>								  	
								</div>
								
							  <div class="tab-pane fade" id="instructor" role="tabpanel" aria-labelledby="instructor-tab">
								  <div class="blog-author instructor-profile">
									<div class="media">
									  <img src="{{ asset('images/c6d02f9cec400bee8cff.png') }}" alt="Generic placeholder image">
									  <div class="media-body">
										<h5>{{ $tutorial->author->name }}</h5>
										<p>{{ $tutorial->author->biography }}</p>							    
										  <div class="social-link">
											<ul>
												<li class="facebook"><a href="{{ $tutorial->author->facebook }}" target="_blank"> <i class="fab fa-facebook-f"></i> </a></li>
												<li class="instagram"><a href="{{ $tutorial->author->instagram }}" target="_blank"> <i class="fab fa-instagram"></i> </a></li>											
												<li class="twitter"><a href="{{ $tutorial->author->twitter }}" target="_blank"> <i class="fab fa-twitter"></i> </a></li>
											</ul>					
										</div>
									  </div>
									</div>							
								</div>
							  </div>
							  <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">
								  <div class="review-bar">
									<div class="progress mb-3">
									  <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
									</div>
									<div class="progress mb-3">
									  <div class="progress-bar" role="progressbar" style="width: 50%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">50%</div>
									</div>
									<div class="progress mb-3">
									  <div class="progress-bar" role="progressbar" style="width: 75%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">75%</div>
									</div>
									<div class="progress mb-3">
									  <div class="progress-bar" role="progressbar" style="width: 80%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">80%</div>
									</div>
									<div class="progress mb-3">
									  <div class="progress-bar" role="progressbar" style="width: 45%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">45%</div>
									</div>
								</div>
							  </div>
							</div>
						</div>

					</div>
				</div>				
				<div class="col-md-4">
					<div class="course-details-sidebar">
						<div class="course-feature">
							<h2>ميزات الدورة</h2>
							<ul>
								<li> عدد الدروس <i class="far fa-file"></i> <span> {{ count($tutorial->lesson) }} </span></li>
								<li> مدة الدرس بالساعة <i class="far fa-clock"></i> <span>  {{ $tutorial->total_content_length }}  </span></li>
								<li> مدة الولوج <i class="far fa-clock"></i> <span> {{ $tutorial->access_type }}  </span></li>
								<li> عدد الطلاب <i class="far fa-user"></i> <span> {{ count($tutorial->purchased) }} </span></li>
								<li> شهادة <i class="fas fa-certificate"></i> 
									<span> 
										@if ($tutorial->is_certificate)
												نعم
										@else
												لا
										@endif
									</span>
								</li>
								<li> تصنيف <i class="far fa-bookmark"></i> <span> {{ $tutorial->category->name }} </span></li>
								<li> المستوى <i class="far fa-lightbulb"></i> <span> {{ $tutorial->the_level }} </span></li>
							</ul>
						</div>
						<div class="footer-social-link">
							<h2>شارك عبر</h2>
							<ul>
								<li><a href="#"> <i class="fab fa-facebook-f"></i> </a></li>	
								<li><a href="#"> <i class="fab fa-instagram"></i> </a></li>		
								<li><a href="#"> <i class="fab fa-twitter"></i> </a></li>
								<li><a href="#"> <i class="fab fa-google-plus-g"></i> </a></li>
							</ul>					
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="related-course-2x">
		<div class="container">
			<div class="col-md-12">
				<div class="row">
					<div class="title-left">
						<h2>الدورات ذات الصلة</h2>
					</div>
				</div>
			</div>

			<div class="related-course">

				@foreach ($tutorial->category->tutorial as $tutorial_category_tutorial_item)
						<div class="single-course">
							<div class="hvrbox">
								<img src="{{ $tutorial_category_tutorial_item->imageurl }}" alt="{{ $tutorial_category_tutorial_item->title }}" class="hvrbox-layer_bottom">
								<div class="hvrbox-layer_top hvrbox-text">
									<div class="hvrbox-text">																	
										<a href="{{ $tutorial_category_tutorial_item->videotrialurl }}" class="btn-circle video"><i class="fas fa-play"></i></a><br>
										<a href="/tutorial/{{ $tutorial_category_tutorial_item->id }}">معاينة الدورة</a>	
									</div>
								</div>
							</div>
							<div class="single-course-content">
							<a href="/tutorial/{{ $tutorial_category_tutorial_item->id }}">{{ $tutorial_category_tutorial_item->title }}</a>
								<p> {{ $tutorial_category_tutorial_item->author->name }} <span><del>{{ $tutorial_category_tutorial_item->price }}</del> <b>السعر : {{ $tutorial_category_tutorial_item->price }}</b></span></p>
								<h3>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i> (4)
									<span>المشتركين : 140</span>
								</h3>
							</div>
						</div>
				@endforeach

			</div>
		</div>
	</div>

</div>


<div class="cta-1x">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="cta-content">
					<h3>هل أنت جاهز للبدء؟</h3>
					<p>ابحث عن الموضوعات التي تحبها عن طريق تصفح فئات الدورات التدريبية عبر الإنترنت <br> ابدأ التعلم مع أفضل الدورات التي تم إنشاؤها باستخدام خبراء </p>
					<a href="#" class="btn-small">ابدأ التدريس</a>
					<a href="#" class="btn-small">ابدا بالتعلم</a>
				</div>
			</div>					
		</div>
	</div>
</div>

@endsection
