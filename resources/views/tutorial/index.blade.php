@extends('layouts.app')

@section('content')
<div class="main-banner course-list-banner">           			
    <div class="hvrbox">
        <img src="{{ asset('images/banner-1.png') }}" alt="Mountains" class="hvrbox-layer_bottom">
        <div class="hvrbox-layer_top">
            <div class="container">
                <div class="overlay-text text-center">						
                    <h3>لدينا أكثر من <b>{{ count($tutorial_all) }}</b> دورة ودرس </h3>
                    <div class="col-md-8 offset-md-2">
                        <form action="" method="get">
                            <div class="input-group">
                                <input value="<?php echo isset($_GET['textrequest']) ? $_GET['textrequest'] : "" ?>" name="textrequest" type="text" class="form-control" aria-label="Text input with dropdown button" placeholder="أدخل بحثك هنا">
                                <div class="input-group-append">
                                    <button class="btn btn-search" type="submit"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>			
                </div>
            </div>
        </div>
    </div>	                     
</div>

<div class="course-header-1x">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <a href="{{ URL::current() }}" class="course-header-left-top">
                <p><img src="{{ asset('images/cross.png') }}" alt="Image"> إزالة البحث </p>
                </a>
                <div class="course-header-left">
                    <div id="accordion">
                      <div class="card">
                        <div class="card-header" id="headingOne">
                            <a href="#" class="icon-right" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              <h3>الفئات</h3>
                            </a>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                          <div class="card-body">
                                <div class="category">				
                                    <ul>
                                        <li><a href="{{ Request::fullUrlWithQuery(['category' => '0', 'page' => '1']) }}"><img src="{{ asset('images/arrow-right.png') }}" alt="Image"> جميع الفئات <span>{{ count($category_all) }}</span></a></li>
                                        @foreach ($category_all as $category_all_item)
                                            <li><a href="{{ Request::fullUrlWithQuery(['category' => $category_all_item->id, 'page' => '1']) }}"><img src="{{ asset('images/arrow-right.png') }}" alt="Image"> {{ $category_all_item->name }} <span>{{ count($category_all_item->tutorial) }}</span></a></li>
                                        @endforeach
                                    </ul>
                                </div>
                          </div>
                        </div>
                      </div>

                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="course-header-right">
                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="search-box d-flex flex-row">
                              <p>  </p>	
                               
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="course-tab">
                                <p>جميع الأسعار بالريال</p>
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                  <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fas fa-list"></i></a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i class="fas fa-th"></i></a>
                                  </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="course-grid-list">
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="all-course-list">
                                
                                @foreach ($tutorial as $tutorial_item)
                                    <div class="media">
                                        <div class="media-left-image">
                                            <div class="hvrbox">
                                                <img src="{{ $tutorial_item->imageurl }}" alt="{{ $tutorial_item->title }}" class="hvrbox-layer_bottom">
                                                <div class="hvrbox-layer_top hvrbox-text">
                                                    <div class="hvrbox-text">																	
                                                        <a href="{{ $tutorial_item->videotrialurl }}" class="btn-circle video"><i class="fas fa-play"></i></a><br>
                                                        <a href="course-single-one.html">معاينة الدورة</a>	
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <h2><a href="/tutorial/{{ $tutorial_item->id }}">{{ $tutorial_item->title }}</a></h2>
                                            <h5>{{ $tutorial_item->author->name }}</h5>
                                            <h4>{{ $tutorial_item->price }} <del>{{ $tutorial_item->price * 2 }}</del></h4>
                                            <a href="course-single-one.html" class="btn-bordered"> معاينة الدورة </a>
                                            <h3>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i> (4)
                                                <span>المشتركين : {{ count($tutorial_item->purchased) }} <i class="far fa-heart"></i></span>
                                            </h3>											
                                        </div>
                                    </div>
                                @endforeach
                                    
                                {{ $tutorial->appends(Request::all())->links() }}

                                <div class="course-pagination" style="display:none;">
                                    <ul class="pagination">	    
                                        <li class="page-item active"><span class="page-link">1</span></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                                        <li class="page-item"><a class="page-link" href="#"><i class="fas fa-angle-right"></i></a></li>
                                    </ul>
                                </div>									
                                
                            </div>
                      </div>

                      <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="course-1x all-course-grid">
                            <div class="container">
                                <div class="row">

                                        <div class="all-course">
                                            <div class="row">

                                                @foreach ($tutorial as $tutorial_item)
                                                    <div class="col-md-4 tile web">
                                                        <div class="single-course">
                                                            <div class="hvrbox">
                                                                <img src="{{ $tutorial_item->imageurl }}" alt="{{ $tutorial_item->title }}" class="hvrbox-layer_bottom">
                                                                <div class="hvrbox-layer_top hvrbox-text">
                                                                    <div class="hvrbox-text">																	
                                                                        <a href="{{ $tutorial_item->videotrialurl }}" class="btn-circle video"><i class="fas fa-play"></i></a><br>
                                                                        <a href="/tutorial/{{ $tutorial_item->id }}">معاينة الدورة</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="single-course-content">
                                                                <a href="/tutorial/{{ $tutorial_item->id }}">{{ $tutorial_item->title }}</a>
                                                                <p> {{ $tutorial_item->author->name }} <span><del>{{ $tutorial_item->price }}</del> <b>السعر : {{ $tutorial_item->price }}</b></span></p>
                                                                <h3>
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i> (4)
                                                                    <span>المشتركين : {{ count($tutorial_item->purchased) }}</span>
                                                                </h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
            
                                                <div class="col-md-12">
                                                    {{ $tutorial->appends(Request::all())->links() }} 
                                                </div>

                                            </div>
                                        </div>


                                </div>
                            </div>
                        </div>					  	
                      </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
@endsection