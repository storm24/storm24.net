@extends('layouts.appadmin')

@section('content')
 <div class="m-content">
     <!--begin:: Widgets/Stats-->
     <div class="m-portlet ">
            <div class="m-portlet__body  m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-6 col-xl-3">

                        <!--begin::Total Profit-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    اجمالي الربح
                                </h4><br>
                                <span class="m-widget24__desc">
                                    شاملة جميع المصاريف
                                </span>
                                <span class="m-widget24__stats m--font-brand">
                                    {{ $all_purchased->sum('price') }}
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-brand" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    نسبة مؤوية
                                </span>
                                <span class="m-widget24__number">
                                    78%
                                </span>
                            </div>
                        </div>

                        <!--end::Total Profit-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">

                        <!--begin::New Feedbacks-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    طلبات هذا اليوم
                                </h4><br>
                                <span class="m-widget24__desc">
                                    إجمالي الطلبات خلال يوم واحد
                                </span>
                                <span class="m-widget24__stats m--font-info">
                                    1349
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-info" role="progressbar" style="width: 84%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    نسبة مؤوية
                                </span>
                                <span class="m-widget24__number">
                                    84%
                                </span>
                            </div>
                        </div>

                        <!--end::New Feedbacks-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">

                        <!--begin::New Orders-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    أرباح هذا الشهر
                                </h4><br>
                                <span class="m-widget24__desc">
                                    مجموع أرباح هذا الشهر
                                </span>
                                <span class="m-widget24__stats m--font-danger">
                                    567
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-danger" role="progressbar" style="width: 69%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    نسبة مؤوية
                                </span>
                                <span class="m-widget24__number">
                                    69%
                                </span>
                            </div>
                        </div>

                        <!--end::New Orders-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">

                        <!--begin::New Users-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    مستخدمون جدد
                                </h4><br>
                                <span class="m-widget24__desc">
                                    الطلاب المنضمين للبرنامج
                                </span>
                                <span class="m-widget24__stats m--font-success">
                                    {{ count($all_student) }}
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-success" role="progressbar" style="width: 90%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    نسبة مؤوية
                                </span>
                                <span class="m-widget24__number">
                                    90%
                                </span>
                            </div>
                        </div>

                        <!--end::New Users-->
                    </div>
                </div>
            </div>
        </div>

        <!--end:: Widgets/Stats-->
 </div>
@endsection
