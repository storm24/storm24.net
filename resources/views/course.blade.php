@extends('layouts.app')

@section('content')

@include('layouts.includes.mainbanner')

<div class="course-single-body">
	<div class="container">
		<div class="course-info-1x course-info-2x">	
			<div class="row">	
				<div class="col-md-4">
					<div class="course-info-left">
						<div class="media">
						  <img src="images/testimonial-1.jpg" alt="Image">
						  <div class="media-body">
							  <h3>Sara Tylor</h3>
							<p>Visual Instructor</p>
						  </div>
						</div>						
					</div>
				</div>	
				<div class="col-md-3">
					<div class="course-info-middle">
						<p>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							&nbsp; 4.9 (66 تقييمات)
						</p>
						<h4>1,457 الطلاب المسجلين</h4>						
					</div>
				</div>
				<div class="col-md-2">
					<div class="course-info-left">
						<div class="media">
						  <div class="media-body">
							  <h4>مدة الدورة</h4>
							<p>3 Weeks</p>
						  </div>
						</div>						
					</div>
				</div>
				<div class="col-md-3">
					<div class="course-info-right">
						<h3><del>$59</del>$169</h3>
						<a href="#" class="btn-small">إشترك الأن</a>
					</div>
				</div>				

			</div>
		</div>
	</div>

	<div class="container">	
		<div class="course-details-1x">
			<div class="row">
				<div class="col-md-8">
					<div class="course-details-left">
						<div class="course-video">
							<a href="https://www.youtube.com/watch?v=gwinFP8_qIM" class="btn-circle video"><i class="fas fa-play"></i></a>
							<h3>معاينة الدورة</h3>
						</div>
					

						<div class="course-menu-nav">
							<ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">

								<li class="nav-item">
											<a class="nav-link" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review" aria-selected="false"><i class="far fa-comment"></i> تقييمات المستخدمين </a>
								</li>
								<li class="nav-item">
										<a class="nav-link" id="instructor-tab" data-toggle="tab" href="#instructor" role="tab" aria-controls="instructor" aria-selected="false"><i class="far fa-user"></i> المدرب</a>
								</li>

							  <li class="nav-item">
									<a class="nav-link" id="curriculum-tab" data-toggle="tab" href="#curriculum" role="tab" aria-controls="curriculum" aria-selected="false"><i class="fas fa-cube"></i> المنهاج </a>
							  </li>
							  <li class="nav-item">
										<a class="nav-link active" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true"> <i class="far fa-file-alt"></i>  نظرة عامة</a>
								</li>
							  
							</ul>
							<div class="tab-content course-menu-tab" id="myTabContent">
							  <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview-tab">
								  <div class="learning-system">
									  <h4>What Will I Learn?</h4>
									  <p>Learn cutting-edge deep reinforcement learning algorithms—from Deep Q-Networks (DQN) to Deep Deterministic Policy Gradients (DDPG). Apply these concepts to train agents to walk, drive, or perform other complex tasks.</p>
									</div>
									<div class="price">
											<h5><del>$169</del><span>$139</span></h5>							  		
											<a href="#" class="btn-small">Buy Now</a>
									</div>
								</div>
								
							  <div class="tab-pane fade" id="curriculum" role="tabpanel" aria-labelledby="curriculum-tab">
										<div class="learning-system">
												<h4>What Will I Learn?</h4>
												<p>Learn cutting-edge deep reinforcement learning algorithms—from Deep Q-Networks (DQN) to Deep Deterministic Policy Gradients (DDPG). Apply these concepts to train agents to walk, drive, or perform other complex tasks.</p>
											</div>
											<div class="price">
													<h5><del>$169</del><span>$139</span></h5>							  		
													<a href="#" class="btn-small">Buy Now</a>
											</div>								  	
								</div>
								
							  <div class="tab-pane fade" id="instructor" role="tabpanel" aria-labelledby="instructor-tab">
								  <div class="blog-author instructor-profile">
									<div class="media">
									  <img src="images/speaker-1.png" alt="Generic placeholder image">
									  <div class="media-body">
										<h5>Dr. Stavens Madison</h5>
										<p>He attended and graduated from medical school in 1976, having over 42 years of diverse experience, especially in Cardiovascular Disease (Cardiology).</p>							    
										  <div class="social-link">
											<ul>
												<li class="facebook"><a href="#"> <i class="fab fa-facebook-f"></i> </a></li>
												<li class="pinterest"><a href="#"> <i class="fab fa-pinterest"></i> </a></li>	
												<li class="instagram"><a href="#"> <i class="fab fa-instagram"></i> </a></li>											
												<li class="twitter"><a href="#"> <i class="fab fa-twitter"></i> </a></li>
											</ul>					
										</div>
									  </div>
									</div>							
								</div>
							  </div>
							  <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">
								  <div class="review-bar">
									<div class="progress mb-3">
									  <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
									</div>
									<div class="progress mb-3">
									  <div class="progress-bar" role="progressbar" style="width: 50%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">50%</div>
									</div>
									<div class="progress mb-3">
									  <div class="progress-bar" role="progressbar" style="width: 75%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">75%</div>
									</div>
									<div class="progress mb-3">
									  <div class="progress-bar" role="progressbar" style="width: 80%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">80%</div>
									</div>
									<div class="progress mb-3">
									  <div class="progress-bar" role="progressbar" style="width: 45%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">45%</div>
									</div>
								</div>
							  </div>
							</div>
						</div>

					</div>
				</div>				
				<div class="col-md-4">
					<div class="course-details-sidebar">
						<div class="course-feature">
							<h2>ميزات الدورة</h2>
							<ul>
								<li> عدد الدروس <i class="far fa-file"></i> <span> 7 </span></li>
								<li> مدة الدرس بالساعة <i class="far fa-clock"></i> <span>  8  </span></li>
								<li> مدة الولوج <i class="far fa-clock"></i> <span> مدى الحياة  </span></li>
								<li> عدد الطلاب <i class="far fa-user"></i> <span> 7 </span></li>
								<li> شهادة <i class="fas fa-certificate"></i> <span> 7 </span></li>
								<li> تصنيف <i class="far fa-bookmark"></i> <span> 7 </span></li>
								<li> المستوى <i class="far fa-lightbulb"></i> <span> مبتدئ </span></li>
							</ul>
						</div>
						<div class="footer-social-link">
							<h2>شارك عبر</h2>
							<ul>
								<li><a href="#"> <i class="fab fa-facebook-f"></i> </a></li>	
								<li><a href="#"> <i class="fab fa-instagram"></i> </a></li>		
								<li><a href="#"> <i class="fab fa-twitter"></i> </a></li>
								<li><a href="#"> <i class="fab fa-google-plus-g"></i> </a></li>
							</ul>					
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="related-course-2x">
		<div class="container">
			<div class="col-md-12">
				<div class="row">
					<div class="title-left">
						<h2>الدورات ذات الصلة</h2>
					</div>
				</div>
			</div>

			<div class="related-course">					
				<div class="single-course">
					<div class="hvrbox">
						<img src="images/2.jpg" alt="slide 1" class="hvrbox-layer_bottom">
						<div class="hvrbox-layer_top hvrbox-text">
							<div class="hvrbox-text">																	
								<a href="https://www.youtube.com/watch?v=gwinFP8_qIM" class="btn-circle video"><i class="fas fa-play"></i></a><br>
								<a href="course-single-one.html">معاينة الدورة</a>	
							</div>
						</div>
					</div>
					<div class="single-course-content">
						<a href="course-single-one.html">Visual Basic Essential Training</a>
						<p>Nir Eyal <span><del>$169</del> <b>$149</b></span></p>
						<h3>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i> (4)
							<span>Enroll: 128</span>
						</h3>
					</div>
				</div>
				<div class="single-course">
					<div class="hvrbox">
						<img src="images/6.jpg" alt="slide 1" class="hvrbox-layer_bottom">
						<div class="hvrbox-layer_top hvrbox-text">
							<div class="hvrbox-text">																	
								<a href="https://www.youtube.com/watch?v=gwinFP8_qIM" class="btn-circle video"><i class="fas fa-play"></i></a><br>
								<a href="course-single-one.html">معاينة الدورة</a>	
							</div>
						</div>
					</div>
					<div class="single-course-content">
						<a href="course-single-one.html">Complete Cyber Security Course</a>
						<p>Nir Eyal <span><del>$169</del> <b>$149</b></span></p>
						<h3>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i> (4)
							<span>Enroll: 128</span>
						</h3>
					</div>
				</div>
				<div class="single-course">
					<div class="hvrbox">
						<img src="images/3.jpg" alt="slide 1" class="hvrbox-layer_bottom">
						<div class="hvrbox-layer_top hvrbox-text">
							<div class="hvrbox-text">																	
								<a href="https://www.youtube.com/watch?v=gwinFP8_qIM" class="btn-circle video"><i class="fas fa-play"></i></a><br>
								<a href="course-single-one.html">معاينة الدورة</a>	
							</div>
						</div>
					</div>
					<div class="single-course-content">
						<a href="course-single-one.html">The Complete Developer Web Course</a>
						<p>Nir Eyal <span><del>$169</del> <b>$149</b></span></p>
						<h3>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i> (4)
							<span>Enroll: 128</span>
						</h3>
					</div>
				</div>
				<div class="single-course">
					<div class="hvrbox">
						<img src="images/4.jpg" alt="slide 1" class="hvrbox-layer_bottom">
						<div class="hvrbox-layer_top hvrbox-text">
							<div class="hvrbox-text">																	
								<a href="https://www.youtube.com/watch?v=gwinFP8_qIM" class="btn-circle video"><i class="fas fa-play"></i></a><br>
								<a href="course-single-one.html">معاينة الدورة</a>	
							</div>
						</div>
					</div>
					<div class="single-course-content">
						<a href="course-single-one.html">Creating 3D environment Blender</a>
						<p>Nir Eyal <span><del>$169</del> <b>$149</b></span></p>
						<h3>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i> (4)
							<span>Enroll: 128</span>
						</h3>
					</div>
				</div>
				<div class="single-course">
					<div class="hvrbox">
						<img src="images/5.jpg" alt="slide 1" class="hvrbox-layer_bottom">
						<div class="hvrbox-layer_top hvrbox-text">
							<div class="hvrbox-text">																	
								<a href="https://www.youtube.com/watch?v=gwinFP8_qIM" class="btn-circle video"><i class="fas fa-play"></i></a><br>
								<a href="course-single-one.html">معاينة الدورة</a>	
							</div>
						</div>
					</div>
					<div class="single-course-content">
						<a href="course-single-one.html">Advanced CSS and Sass Flexbox Grid</a>
						<p>Nir Eyal <span><del>$169</del> <b>$149</b></span></p>
						<h3>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i> (4)
							<span>Enroll: 128</span>
						</h3>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>


<div class="cta-1x">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="cta-content">
					<h3>هل أنت جاهز للبدء؟</h3>
					<p>ابحث عن الموضوعات التي تحبها عن طريق تصفح فئات الدورات التدريبية عبر الإنترنت <br> ابدأ التعلم مع أفضل الدورات التي تم إنشاؤها باستخدام خبراء </p>
					<a href="#" class="btn-small">ابدأ التدريس</a>
					<a href="#" class="btn-small">ابدا بالتعلم</a>
				</div>
			</div>					
		</div>
	</div>
</div>

@endsection
