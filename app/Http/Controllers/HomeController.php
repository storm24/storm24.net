<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Purchased;
use App\student;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $all_purchased = Purchased::get();
        $all_student = student::get();
        return view('home', compact('all_purchased', 'all_student'));
    }
}
