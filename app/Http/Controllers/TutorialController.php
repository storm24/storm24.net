<?php

namespace App\Http\Controllers;

use App\tutorial;
use Illuminate\Http\Request;
use App\category;

class TutorialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tutorial  $tutorial
     * @return \Illuminate\Http\Response
     */
    public function show(tutorial $tutorial)
    {
        return view('client.edit', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tutorial  $tutorial
     * @return \Illuminate\Http\Response
     */
    public function edit(tutorial $tutorial)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tutorial  $tutorial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tutorial $tutorial)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tutorial  $tutorial
     * @return \Illuminate\Http\Response
     */
    public function destroy(tutorial $tutorial)
    {
        //
    }


    //author category tutorial
    public function searchtutorial(Request $request)
    {
        //dd($request->ggg); overview_body curriculum_body  textrequest category
        $tutorial_all = tutorial::get();

        $itemsNumber = 3;
        if(isset($request->category) && $request->category != 0){
            $tutorial = tutorial::where('categorie_id', '=', $request->category)
                ->where(function($query) use ($request) {
                    $query->where('title', 'LIKE', '%' . $request->textrequest . '%' )
                    ->orWhere('overview_body', 'LIKE', '%' . $request->textrequest . '%' )
                    ->orWhere('curriculum_body', 'LIKE', '%' . $request->textrequest . '%' );
                })
                ->paginate($itemsNumber);
        }else{
            $tutorial = tutorial::where('title', 'LIKE', '%' . $request->textrequest . '%' )->orWhere('overview_body', 'LIKE', '%' . $request->textrequest . '%' )->orWhere('curriculum_body', 'LIKE', '%' . $request->textrequest . '%' )->paginate($itemsNumber);
        }

        $category_all = category::get();

        return view('tutorial.index', compact('tutorial', 'category_all', 'tutorial_all'));
    }

    public function showtutorial(tutorial $tutorial)
    {
        if($tutorial->is_active){
            return view('tutorial.show', compact('tutorial'));
        }else{
            return abort(404);
        }
    }

}
