<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tutorial extends Model
{
    public function author()
    {
        return $this->belongsTo('App\author');
    }

    public function category()
    {
        return $this->belongsTo('App\category', "categorie_id");
    }

    public function lesson()
    {
        return $this->hasMany('App\lesson');
    }

    public function purchased()
    {
        return $this->hasMany('App\Purchased');
    }

    protected $fillable = [
        "title", "overview_body", "curriculum_body", "imageurl", "videotrialurl", "total_content_length", "price", "access_type", "is_certificate", "the_level", "is_active", "author_id", "categorie_id"
    ];
}
