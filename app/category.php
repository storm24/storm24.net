<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    public function tutorial()
    {
        return $this->hasMany('App\tutorial', "categorie_id");
    }

    protected $fillable = [
        "name"
    ];

}
