<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchaseds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('price')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::table('purchaseds', function (Blueprint $table) {
            //$table->engine = 'InnoDB';
            $table->bigInteger('student_id')->unsigned();
            $table->bigInteger('tutorial_id')->unsigned();
        
            $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
            $table->foreign('tutorial_id')->references('id')->on('tutorials')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchaseds');
    }
}
