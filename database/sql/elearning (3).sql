-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 13, 2019 at 07:29 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elearning`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `announcement_body` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biography` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`id`, `name`, `job`, `facebook`, `twitter`, `instagram`, `biography`, `created_at`, `updated_at`) VALUES
(1, 'محمد أبي القاسم', 'مبرمج ومحاضر', 'https://twitter.com/honaalhal?lang=ar', 'https://twitter.com/honaalhal?lang=ar', 'https://twitter.com/honaalhal?lang=ar', 'أنا الفقير إلى الله تعالى (أبو عمر الباحث) مسلم سُنِّي على منهج السلف الصالح.\r\nأهتم بالقرآن الكريم وحديث رسول الله صلى الله عليه وسلم\r\nخصصتُ حياتي للدفاع عن كتاب الله جلّ في علاه وسنة رسوله ومصطفاه\r\nأرجو ثواب الله والدار الآخرة\r\nقال شيخ الإسلام ابنُ تيمية:\r\n{ فالرَّادُّ على أهل البدع مجاهد ، حتى كان يَحيى بن يَحيى يقول : الذَّبُّ عن السُّنة أفضلُ مِن الجهاد }مجموع الفتاوى ج4 ص13\r\nولا أجد عملاً أتقرَّبُ به إلى الله تعالى بعد الفروض والواجبات أفضل من هذا العمل.\r\n\r\n\r\nنسأل اللهَ عزَّ وجلَّ الإخلاصَ والقبول.\r\n', '2019-06-09 18:00:00', '2019-06-09 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'التسويق والمبيعات', '2019-06-09 18:00:00', '2019-06-09 18:00:00'),
(2, 'التصميم', '2019-06-09 18:00:00', '2019-06-09 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `lessons`
--

CREATE TABLE `lessons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_length` int(10) UNSIGNED DEFAULT NULL,
  `content_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tutorial_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_06_07_235843_create_authors_table', 1),
(4, '2019_06_07_237953_create_categories_table', 1),
(5, '2019_06_07_238045_create_tutorials_table', 1),
(6, '2019_06_07_238054_create_lessons_table', 1),
(7, '2019_06_10_000923_create_students_table', 1),
(8, '2019_06_10_001413_create_purchaseds_table', 1),
(9, '2019_06_11_040249_create_announcements_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchaseds`
--

CREATE TABLE `purchaseds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `price` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `student_id` bigint(20) UNSIGNED NOT NULL,
  `tutorial_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchaseds`
--

INSERT INTO `purchaseds` (`id`, `price`, `created_at`, `updated_at`, `student_id`, `tutorial_id`) VALUES
(1, 150, '2019-06-09 21:00:00', '2019-06-09 21:00:00', 1, 6),
(2, 150, '2019-06-09 21:00:00', '2019-06-09 21:00:00', 1, 6),
(3, 150, '2019-06-09 21:00:00', '2019-06-09 21:00:00', 2, 5),
(4, 150, '2019-06-09 21:00:00', '2019-06-09 21:00:00', 2, 6),
(5, 150, '2019-06-09 21:00:00', '2019-06-09 21:00:00', 1, 3),
(6, 150, '2019-06-09 21:00:00', '2019-06-09 21:00:00', 2, 6),
(7, 150, '2019-06-09 21:00:00', '2019-06-09 21:00:00', 3, 1),
(8, 150, '2019-06-09 21:00:00', '2019-06-09 21:00:00', 1, 6),
(9, 150, '2019-06-09 21:00:00', '2019-06-09 21:00:00', 3, 6),
(10, 150, '2019-06-09 21:00:00', '2019-06-09 21:00:00', 3, 1),
(11, 150, '2019-06-09 21:00:00', '2019-06-09 21:00:00', 1, 1),
(12, 150, '2019-06-09 21:00:00', '2019-06-09 21:00:00', 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `email`, `created_at`, `updated_at`) VALUES
(1, 'عبد الله', 'aboalkassem@gmail.com', '2019-06-09 21:00:00', '2019-06-09 21:00:00'),
(2, 'عبد الواحد', 'aboalmotana@gmail.com', '2019-06-09 21:00:00', '2019-06-09 21:00:00'),
(3, 'عبد الملك', 'abohoyam@gmail.com', '2019-06-09 21:00:00', '2019-06-09 21:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tutorials`
--

CREATE TABLE `tutorials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `overview_body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `curriculum_body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `imageurl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `videotrialurl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_content_length` int(10) UNSIGNED DEFAULT NULL,
  `price` int(10) UNSIGNED DEFAULT NULL,
  `access_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_certificate` tinyint(1) DEFAULT '0',
  `the_level` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `author_id` bigint(20) UNSIGNED NOT NULL,
  `categorie_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tutorials`
--

INSERT INTO `tutorials` (`id`, `title`, `overview_body`, `curriculum_body`, `imageurl`, `videotrialurl`, `total_content_length`, `price`, `access_type`, `is_certificate`, `the_level`, `is_active`, `created_at`, `updated_at`, `author_id`, `categorie_id`) VALUES
(1, 'قوة تأثير المؤثّرين في حملاتك التسويقية', '\r\nيبحث المسوّقون دائمًا عن طرق مبتكرة لتوسيع نطاق وصولهم وزيادة التحويلات. ولكن قد يكون من الصعب العثور على استراتيجيات استهداف مناسبة للعملاء الذين ترغب في جذبهم.', 'الوصول الفوري لجمهور واسع\r\nعندما تبدأ حملة تسويق رقمية، ينبغي أن تركّز مرحلة التخطيط على تطوير ملفات شخصية لمختلف أنواع العملاء الذين تهدف إلى التقاطهم. يمكن أن ينجح إنشاء شبكة واسعة من الجمهور ذي الاهتمام البسيط بمجالك في بعض الأحيان، ولكن في الأيام الأولى لبدء تشغيل النشاط التجاري أو للشركات التي تعمل في مجالات متخصّصة ضيقة التركيز، يكون التسويق المستهدف أكثر فعالية.', 'https://academy.hsoub.com/uploads/monthly_2017_12/5a212a505187f_01-2(1).png.e5d4a873c0583ba73ab0d980e299ddf9.png', 'https://www.youtube.com/watch?v=jy6I-oDT-4Q', 30, 370, 'مدى الحياة', 1, 'متوسط', 1, '2019-06-09 18:00:00', '2019-06-09 18:00:00', 1, 1),
(2, 'قوة تأثير المؤثّرين في حملاتك التسويقية', '\r\nيبحث المسوّقون دائمًا عن طرق مبتكرة لتوسيع نطاق وصولهم وزيادة التحويلات. ولكن قد يكون من الصعب العثور على استراتيجيات استهداف مناسبة للعملاء الذين ترغب في جذبهم.', 'الوصول الفوري لجمهور واسع\r\nعندما تبدأ حملة تسويق رقمية، ينبغي أن تركّز مرحلة التخطيط على تطوير ملفات شخصية لمختلف أنواع العملاء الذين تهدف إلى التقاطهم. يمكن أن ينجح إنشاء شبكة واسعة من الجمهور ذي الاهتمام البسيط بمجالك في بعض الأحيان، ولكن في الأيام الأولى لبدء تشغيل النشاط التجاري أو للشركات التي تعمل في مجالات متخصّصة ضيقة التركيز، يكون التسويق المستهدف أكثر فعالية.', 'https://academy.hsoub.com/uploads/monthly_2017_09/59c5119e74c7a_main(47).png.86b46100c07dadf2f6fea40bb57b765b.png', 'https://www.youtube.com/watch?v=sngQiOXMaWs', 30, 370, 'مدى الحياة', 1, 'متوسط', 1, '2019-06-09 18:00:00', '2019-06-09 18:00:00', 1, 1),
(3, 'قوة تأثير المؤثّرين في حملاتك التسويقية', '\r\nيبحث المسوّقون دائمًا عن طرق مبتكرة لتوسيع نطاق وصولهم وزيادة التحويلات. ولكن قد يكون من الصعب العثور على استراتيجيات استهداف مناسبة للعملاء الذين ترغب في جذبهم.', 'الوصول الفوري لجمهور واسع\r\nعندما تبدأ حملة تسويق رقمية، ينبغي أن تركّز مرحلة التخطيط على تطوير ملفات شخصية لمختلف أنواع العملاء الذين تهدف إلى التقاطهم. يمكن أن ينجح إنشاء شبكة واسعة من الجمهور ذي الاهتمام البسيط بمجالك في بعض الأحيان، ولكن في الأيام الأولى لبدء تشغيل النشاط التجاري أو للشركات التي تعمل في مجالات متخصّصة ضيقة التركيز، يكون التسويق المستهدف أكثر فعالية.', 'https://academy.hsoub.com/uploads/monthly_2017_08/59a512a0c139b_main(5).png.f434416a07ba813a3ad63a446f3c975c.png', '', 30, 370, 'مدى الحياة', 1, 'متوسط', 1, '2019-06-09 18:00:00', '2019-06-09 18:00:00', 1, 1),
(4, 'قوة تأثير المؤثّرين في حملاتك التسويقية', '\r\nيبحث المسوّقون دائمًا عن طرق مبتكرة لتوسيع نطاق وصولهم وزيادة التحويلات. ولكن قد يكون من الصعب العثور على استراتيجيات استهداف مناسبة للعملاء الذين ترغب في جذبهم.', 'الوصول الفوري لجمهور واسع\r\nعندما تبدأ حملة تسويق رقمية، ينبغي أن تركّز مرحلة التخطيط على تطوير ملفات شخصية لمختلف أنواع العملاء الذين تهدف إلى التقاطهم. يمكن أن ينجح إنشاء شبكة واسعة من الجمهور ذي الاهتمام البسيط بمجالك في بعض الأحيان، ولكن في الأيام الأولى لبدء تشغيل النشاط التجاري أو للشركات التي تعمل في مجالات متخصّصة ضيقة التركيز، يكون التسويق المستهدف أكثر فعالية.', 'https://academy.hsoub.com/uploads/monthly_2017_12/5a212a505187f_01-2(1).png.e5d4a873c0583ba73ab0d980e299ddf9.png', 'https://www.youtube.com/watch?v=jy6I-oDT-4Q', 30, 370, 'مدى الحياة', 1, 'متوسط', 1, '2019-06-09 18:00:00', '2019-06-09 18:00:00', 1, 1),
(5, 'قوة تأثير المؤثّرين في حملاتك التسويقية', '\r\nيبحث المسوّقون دائمًا عن طرق مبتكرة لتوسيع نطاق وصولهم وزيادة التحويلات. ولكن قد يكون من الصعب العثور على استراتيجيات استهداف مناسبة للعملاء الذين ترغب في جذبهم.', 'الوصول الفوري لجمهور واسع\r\nعندما تبدأ حملة تسويق رقمية، ينبغي أن تركّز مرحلة التخطيط على تطوير ملفات شخصية لمختلف أنواع العملاء الذين تهدف إلى التقاطهم. يمكن أن ينجح إنشاء شبكة واسعة من الجمهور ذي الاهتمام البسيط بمجالك في بعض الأحيان، ولكن في الأيام الأولى لبدء تشغيل النشاط التجاري أو للشركات التي تعمل في مجالات متخصّصة ضيقة التركيز، يكون التسويق المستهدف أكثر فعالية.', '', 'https://www.youtube.com/watch?v=sngQiOXMaWs', 30, 370, 'مدى الحياة', 1, 'متوسط', 1, '2019-06-09 18:00:00', '2019-06-09 18:00:00', 1, 1),
(6, 'قوة تأثير المؤثّرين في حملاتك التسويقية', '\r\nيبحث المسوّقون دائمًا عن طرق مبتكرة لتوسيع نطاق وصولهم وزيادة التحويلات. ولكن قد يكون من الصعب العثور على استراتيجيات استهداف مناسبة للعملاء الذين ترغب في جذبهم.', 'الوصول الفوري لجمهور واسع\r\nعندما تبدأ حملة تسويق رقمية، ينبغي أن تركّز مرحلة التخطيط على تطوير ملفات شخصية لمختلف أنواع العملاء الذين تهدف إلى التقاطهم. يمكن أن ينجح إنشاء شبكة واسعة من الجمهور ذي الاهتمام البسيط بمجالك في بعض الأحيان، ولكن في الأيام الأولى لبدء تشغيل النشاط التجاري أو للشركات التي تعمل في مجالات متخصّصة ضيقة التركيز، يكون التسويق المستهدف أكثر فعالية.', 'https://academy.hsoub.com/uploads/monthly_2017_08/59912c3d21898_main(4).png.8b69a4b171bc0a54605ca2f6618b19be.png', '', 30, 370, 'مدى الحياة', 1, 'متوسط', 1, '2019-06-09 18:00:00', '2019-06-09 18:00:00', 1, 1),
(7, 'قوة تأثير المؤثّرين في حملاتك التسويقية', '\r\nيبحث المسوّقون دائمًا عن طرق مبتكرة لتوسيع نطاق وصولهم وزيادة التحويلات. ولكن قد يكون من الصعب العثور على استراتيجيات استهداف مناسبة للعملاء الذين ترغب في جذبهم.', 'الوصول الفوري لجمهور واسع\r\nعندما تبدأ حملة تسويق رقمية، ينبغي أن تركّز مرحلة التخطيط على تطوير ملفات شخصية لمختلف أنواع العملاء الذين تهدف إلى التقاطهم. يمكن أن ينجح إنشاء شبكة واسعة من الجمهور ذي الاهتمام البسيط بمجالك في بعض الأحيان، ولكن في الأيام الأولى لبدء تشغيل النشاط التجاري أو للشركات التي تعمل في مجالات متخصّصة ضيقة التركيز، يكون التسويق المستهدف أكثر فعالية.', 'https://academy.hsoub.com/uploads/monthly_2017_08/599daaef59475_main(17).png.b2a006ee104778c88dd71de59f3c49bb.png', '', 30, 370, 'مدى الحياة', 1, 'متوسط', 1, '2019-06-09 18:00:00', '2019-06-09 18:00:00', 1, 1),
(8, 'قوة تأثير المؤثّرين في حملاتك التسويقية', '\r\nيبحث المسوّقون دائمًا عن طرق مبتكرة لتوسيع نطاق وصولهم وزيادة التحويلات. ولكن قد يكون من الصعب العثور على استراتيجيات استهداف مناسبة للعملاء الذين ترغب في جذبهم.', 'الوصول الفوري لجمهور واسع\r\nعندما تبدأ حملة تسويق رقمية، ينبغي أن تركّز مرحلة التخطيط على تطوير ملفات شخصية لمختلف أنواع العملاء الذين تهدف إلى التقاطهم. يمكن أن ينجح إنشاء شبكة واسعة من الجمهور ذي الاهتمام البسيط بمجالك في بعض الأحيان، ولكن في الأيام الأولى لبدء تشغيل النشاط التجاري أو للشركات التي تعمل في مجالات متخصّصة ضيقة التركيز، يكون التسويق المستهدف أكثر فعالية.', 'https://academy.hsoub.com/uploads/monthly_2017_07/main.png.8a8cca46cf5297eb64893ff74d429da9.png', 'https://www.youtube.com/watch?v=sngQiOXMaWs', 30, 370, 'مدى الحياة', 1, 'متوسط', 1, '2019-06-09 18:00:00', '2019-06-09 18:00:00', 1, 1),
(9, 'أمنية تقع في شمال سيناء', '\r\nيبحث المسوّقون دائمًا عن طرق مبتكرة لتوسيع نطاق وصولهم وزيادة التحويلات. ولكن قد يكون من الصعب العثور على استراتيجيات استهداف مناسبة للعملاء الذين ترغب في جذبهم.', 'الوصول الفوري لجمهور واسع\r\nعندما تبدأ حملة تسويق رقمية، ينبغي أن تركّز مرحلة التخطيط على تطوير ملفات شخصية لمختلف أنواع العملاء الذين تهدف إلى التقاطهم. يمكن أن ينجح إنشاء شبكة واسعة من الجمهور ذي الاهتمام البسيط بمجالك في بعض الأحيان، ولكن في الأيام الأولى لبدء تشغيل النشاط التجاري أو للشركات التي تعمل في مجالات متخصّصة ضيقة التركيز، يكون التسويق المستهدف أكثر فعالية.', 'https://academy.hsoub.com/uploads/monthly_2017_12/5a212a505187f_01-2(1).png.e5d4a873c0583ba73ab0d980e299ddf9.png', 'https://www.youtube.com/watch?v=jy6I-oDT-4Q', 30, 370, 'مدى الحياة', 1, 'متوسط', 1, '2019-06-09 18:00:00', '2019-06-09 18:00:00', 1, 1),
(10, 'لأربعاء نقطة تفتيش أمنية في العريش في شمال سيناء', '\r\nيبحث المسوّقون دائمًا عن طرق مبتكرة لتوسيع نطاق وصولهم وزيادة التحويلات. ولكن قد يكون من الصعب العثور على استراتيجيات استهداف مناسبة للعملاء الذين ترغب في جذبهم.', 'الوصول الفوري لجمهور واسع\r\nعندما تبدأ حملة تسويق رقمية، ينبغي أن تركّز مرحلة التخطيط على تطوير ملفات شخصية لمختلف أنواع العملاء الذين تهدف إلى التقاطهم. يمكن أن ينجح إنشاء شبكة واسعة من الجمهور ذي الاهتمام البسيط بمجالك في بعض الأحيان، ولكن في الأيام الأولى لبدء تشغيل النشاط التجاري أو للشركات التي تعمل في مجالات متخصّصة ضيقة التركيز، يكون التسويق المستهدف أكثر فعالية.', 'https://academy.hsoub.com/uploads/monthly_2017_12/5a212a505187f_01-2(1).png.e5d4a873c0583ba73ab0d980e299ddf9.png', 'https://www.youtube.com/watch?v=jy6I-oDT-4Q', 30, 370, 'مدى الحياة', 1, 'متوسط', 1, '2019-06-09 18:00:00', '2019-06-09 18:00:00', 1, 1),
(11, 'سيناء مع المسافر. اكتشف أفضل الأسعار', '\r\nيبحث المسوّقون دائمًا عن طرق مبتكرة لتوسيع نطاق وصولهم وزيادة التحويلات. ولكن قد يكون من الصعب العثور على استراتيجيات استهداف مناسبة للعملاء الذين ترغب في جذبهم.', 'الوصول الفوري لجمهور واسع\r\nعندما تبدأ حملة تسويق رقمية، ينبغي أن تركّز مرحلة التخطيط على تطوير ملفات شخصية لمختلف أنواع العملاء الذين تهدف إلى التقاطهم. يمكن أن ينجح إنشاء شبكة واسعة من الجمهور ذي الاهتمام البسيط بمجالك في بعض الأحيان، ولكن في الأيام الأولى لبدء تشغيل النشاط التجاري أو للشركات التي تعمل في مجالات متخصّصة ضيقة التركيز، يكون التسويق المستهدف أكثر فعالية.', 'https://academy.hsoub.com/uploads/monthly_2017_07/main.png.8a8cca46cf5297eb64893ff74d429da9.png', 'https://www.youtube.com/watch?v=sngQiOXMaWs', 30, 370, 'مدى الحياة', 1, 'متوسط', 1, '2019-06-09 18:00:00', '2019-06-09 18:00:00', 1, 1),
(12, 'قوة تأثير المؤثّرين في حملاتك التسويقية', '\r\nسيناء مع المسافر. اكتشف أفضل الأسعار', 'الوصول الفوري لجمهور واسع\r\nعندما تبدأ حملة تسويق رقمية، ينبغي أن تركّز مرحلة التخطيط على تطوير ملفات شخصية لمختلف أنواع العملاء الذين تهدف إلى التقاطهم. يمكن أن ينجح إنشاء شبكة واسعة من الجمهور ذي الاهتمام البسيط بمجالك في بعض الأحيان، ولكن في الأيام الأولى لبدء تشغيل النشاط التجاري أو للشركات التي تعمل في مجالات متخصّصة ضيقة التركيز، يكون التسويق المستهدف أكثر فعالية.', 'https://academy.hsoub.com/uploads/monthly_2017_08/59a512a0c139b_main(5).png.f434416a07ba813a3ad63a446f3c975c.png', '', 30, 370, 'مدى الحياة', 1, 'متوسط', 1, '2019-06-09 18:00:00', '2019-06-09 18:00:00', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `user_type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'mohamed', 'hd@gmail.com', NULL, '$2y$10$nukTkqrqn6lNbtkoIzGfuutaeXk8fEH5yWCqO.dfZa2WsScZ/NcUW', 'user', NULL, '2019-06-11 01:18:17', '2019-06-11 01:18:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lessons`
--
ALTER TABLE `lessons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lessons_tutorial_id_foreign` (`tutorial_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `purchaseds`
--
ALTER TABLE `purchaseds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchaseds_student_id_foreign` (`student_id`),
  ADD KEY `purchaseds_tutorial_id_foreign` (`tutorial_id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `students_email_unique` (`email`);

--
-- Indexes for table `tutorials`
--
ALTER TABLE `tutorials`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tutorials_author_id_foreign` (`author_id`),
  ADD KEY `tutorials_categorie_id_foreign` (`categorie_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lessons`
--
ALTER TABLE `lessons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `purchaseds`
--
ALTER TABLE `purchaseds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tutorials`
--
ALTER TABLE `tutorials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `lessons`
--
ALTER TABLE `lessons`
  ADD CONSTRAINT `lessons_tutorial_id_foreign` FOREIGN KEY (`tutorial_id`) REFERENCES `tutorials` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `purchaseds`
--
ALTER TABLE `purchaseds`
  ADD CONSTRAINT `purchaseds_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `purchaseds_tutorial_id_foreign` FOREIGN KEY (`tutorial_id`) REFERENCES `tutorials` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tutorials`
--
ALTER TABLE `tutorials`
  ADD CONSTRAINT `tutorials_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tutorials_categorie_id_foreign` FOREIGN KEY (`categorie_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
