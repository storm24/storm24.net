<?php
use App\category;
use App\author;
use App\tutorial;
use App\student;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|tutorial/{tutorial}
*/

Route::get('/', function () {
    $author_all = author::get();
    $tutorial_all = tutorial::get();
    $student_all = student::get();

    $category_all = category::get();

    return view('welcome', compact('author_all', 'tutorial_all','student_all','category_all'));
});

Route::get('/course', function () {
    return view('course');
});

Route::get('tutorial', 'TutorialController@searchtutorial');
Route::get('tutorial/{tutorial}', 'TutorialController@showtutorial');
//Route::resource('/tutorial', 'TutorialController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
